﻿using Helpers;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Poc_Hb.Test.Utils
{
    public class HttpClientHelperMock : IHttpClientHelper
    {
        private readonly string _responseMock;

        public HttpClientHelperMock(string responseMock)
        {
            _responseMock = responseMock;
        }

        public async Task<HttpResponseMessage> GetAsync(Uri uri, string requestUri)
        {
            var result = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(_responseMock)
            };

            await Task.Delay(1).ConfigureAwait(false);

            return result;
        }

        public async Task<HttpResponseMessage> GetAsync(Uri uri, string jwtToken, string requestUri)
        {
            var result = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(_responseMock)
            };

            await Task.Delay(1).ConfigureAwait(false);

            return result;
        }

        public async Task<HttpStatusCode> PostAsync(Uri uri, string jwtToken, string requestUri, object payload)
        {
            await Task.Delay(1).ConfigureAwait(false);

            return HttpStatusCode.OK;
        }
    }
}
