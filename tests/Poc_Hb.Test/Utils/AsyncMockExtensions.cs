﻿using System.Collections.Generic;
using System.Linq;

namespace Poc_Hb.Test.Utils
{
    public static class AsyncMockExtensions
    {
        public static IQueryable<TEntity> CreateMock<TEntity>(this IEnumerable<TEntity> entities)
        {
            return new TestAsyncEnumerable<TEntity>(entities);
        }

        public static IQueryable<TEntity> CreateMock<TEntity>(this IQueryable<TEntity> entities)
        {
            return new TestAsyncEnumerable<TEntity>(entities);
        }
    }
}
