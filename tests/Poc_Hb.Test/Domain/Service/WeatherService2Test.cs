﻿using FluentAssertions;
using Infrastructure.MessageBroker.Interfaces;
using MicroService2.Domain.Service.Dtos;
using MicroService2.Domain.Service.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Poc_Hb.Test.Utils;
using Xunit;

namespace Poc_Hb.Test.Domain.Service
{
    public class WeatherService2Test
    {
        private readonly IMessageBroker _messageBroker;
        private readonly IConfiguration _configuration;
        private readonly ILoggerFactory _loggerFactory;

        private readonly string response200 = "{\"coord\":{\"lon\":-49.07,\"lat\":-26.92},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"base\":\"stations\",\"main\":{\"temp\":24.22,\"feels_like\":25.24,\"temp_min\":22.22,\"temp_max\":25.56,\"pressure\":1020,\"humidity\":61},\"visibility\":10000,\"wind\":{\"speed\":1.5,\"deg\":120},\"clouds\":{\"all\":75},\"dt\":1579368469,\"sys\":{\"type\":1,\"id\":8398,\"country\":\"BR\",\"sunrise\":1579336692,\"sunset\":1579385667},\"timezone\":-10800,\"id\":3469968,\"name\":\"Blumenau\",\"cod\":200}";
        private readonly string response404 = "{\"cod\":\"404\",\"message\":\"city not found\"}";

        public WeatherService2Test()
        {
            _messageBroker = Substitute.For<IMessageBroker>();
            _configuration = Substitute.For<IConfiguration>();
            _loggerFactory = Substitute.For<ILoggerFactory>();

            _configuration.GetSection("Endpoints")["Uri"].Returns("http://www.mock.mck");
            _configuration.GetSection("Endpoints")["Key"].Returns("MOCKEY");

        }

        [Fact]
        public async void Should_call_weather_api_and_receive_a_valid_city_info()
        {
            var httpClientHelperMock = new HttpClientHelperMock(response200);
            var weatherService = new WeatherService(_messageBroker, _configuration, httpClientHelperMock, _loggerFactory);

            var register = new RegisterDto
            {
                CityName = "Blumenau",
                Country = "BR"
            };

            var result = await weatherService.CallAsync(register).ConfigureAwait(false);

            result.Should().BeTrue();
        }

        [Fact]
        public async void Should_call_weather_api_and_not_receive_a_valid_city_info()
        {
            var httpClientHelperMock = new HttpClientHelperMock(response404);
            var weatherService = new WeatherService(_messageBroker, _configuration, httpClientHelperMock, _loggerFactory);

            var register = new RegisterDto();

            var result = await weatherService.CallAsync(register).ConfigureAwait(false);

            result.Should().BeFalse();
        }
    }    
}