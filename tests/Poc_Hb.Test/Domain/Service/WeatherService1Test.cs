﻿using FluentAssertions;
using Infrastructure.MessageBroker.Interfaces;
using MicroService1.Domain.Entities;
using MicroService1.Domain.Interfaces.Repositories;
using MicroService1.Domain.Service.Dtos;
using MicroService1.Domain.Service.Interfaces;
using MicroService1.Domain.Service.Services;
using Microsoft.Extensions.Configuration;
using NSubstitute;
using Poc_Hb.Test.Domain.Service.Builders;
using Poc_Hb.Test.Utils;
using System;
using System.Linq;
using Xunit;

namespace Poc_Hb.Test.Domain.Service
{
    public class WeatherService1Test
    {
        private readonly IWeatherService _weatherService;
        private readonly IWeatherRepository _weatherRepository;
        private readonly IMessageBroker _messageBroker;
        private readonly IConfiguration _configuration;
        public WeatherService1Test()
        {
            _weatherRepository = Substitute.For<IWeatherRepository>();
            _messageBroker = Substitute.For<IMessageBroker>();
            _configuration = Substitute.For<IConfiguration>();

            _weatherService = new WeatherService(_weatherRepository, _messageBroker, _configuration);
        }

        [Fact]
        public async void Should_get_weathers()
        {
            _weatherRepository.GetAll().Returns(WeatherBuilder.FixtureWithInfo().CreateMock());

            var result = await _weatherService.GetWeathers().ConfigureAwait(false);

            result.Should().NotBeEmpty();
            result.Should().HaveCount(2);
            result.ElementAt(0).CityName.Should().Be("Blumenau");
            result.ElementAt(1).CityName.Should().Be("Rio de Janeiro");
        }

        [Fact]
        public async void Should_save_weather()
        {            
            var dto = new WeatherRegisterDtoBuilder()
                .WithCountry("BR")
                .WithName("Blumenau")
                .WithFeels_Like(20)
                .WithHumidity(80)
                .WithPressure(30)
                .WithTemperature(22)
                .WithTemp_max(27)
                .WithTemp_min(25).Build();

            await _weatherService.SaveWeather(dto).ConfigureAwait(false);

            _weatherRepository.Received().Add(Arg.Is<Weather>(p => IsExpectedWeather(p, dto)));
        }

        private bool IsExpectedWeather(Weather weather, WeatherRegisterDto dto)
        {
            return
                weather.CityName == dto.Name &&
                weather.Country == dto.Sys.Country &&
                weather.Temperature == dto.Main.Temp &&
                weather.Maximum == dto.Main.Temp_max &&
                weather.Minimum == dto.Main.Temp_min &&
                weather.FeelsLike == dto.Main.Feels_Like &&
                weather.Pressure == dto.Main.Pressure &&
                weather.Humidity == dto.Main.Humidity;
        }
    }
}