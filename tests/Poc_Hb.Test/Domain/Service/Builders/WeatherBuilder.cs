﻿using MicroService1.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Poc_Hb.Test.Domain.Service.Builders
{
    public class WeatherBuilder
    {
        private readonly Weather _construction = new Weather();

        public Weather Build()
        {
            return _construction;
        }

        private WeatherBuilder Construct(Action<Weather> constructor)
        {
            constructor(_construction);
            return this;
        }

        public WeatherBuilder WithId(long id) => Construct(c => c.Id = id);

        public WeatherBuilder WithName(string name) => Construct(c => c.CityName = name);

        public WeatherBuilder WithCountry(string country) => Construct(c => c.Country = country);


        public static IEnumerable<Weather> FixtureWithInfo()
        {
            return new List<Weather>
            {
                new WeatherBuilder().WithId(1).WithName("Blumenau").WithCountry("BR").Build(),
                new WeatherBuilder().WithId(2).WithName("Rio de Janeiro").WithCountry("BR").Build()
            };
        }
    }
}