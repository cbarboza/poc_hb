﻿using MicroService1.Domain.Service.Dtos;
using System;

namespace Poc_Hb.Test.Domain.Service.Builders
{
    public class WeatherRegisterDtoBuilder
    {
        private readonly WeatherRegisterDto _construction = new WeatherRegisterDto { Main = new MainDto(), Sys = new SysDto() };

        public WeatherRegisterDto Build()
        {
            return _construction;
        }

        private WeatherRegisterDtoBuilder Construct(Action<WeatherRegisterDto> constructor)
        {
            constructor(_construction);
            return this;
        }

        public WeatherRegisterDtoBuilder WithId(long id) => Construct(c => c.Id = id);

        public WeatherRegisterDtoBuilder WithName(string name) => Construct(c => c.Name = name);

        public WeatherRegisterDtoBuilder WithCountry(string country) => Construct(c => c.Sys.Country = country);

        public WeatherRegisterDtoBuilder WithTemperature(long temp) => Construct(c => c.Main.Temp = temp);

        public WeatherRegisterDtoBuilder WithPressure(long pressure) => Construct(c => c.Main.Pressure = pressure);

        public WeatherRegisterDtoBuilder WithFeels_Like(long feels_Like) => Construct(c => c.Main.Feels_Like = feels_Like);

        public WeatherRegisterDtoBuilder WithTemp_max(long temp_max) => Construct(c => c.Main.Temp_max = temp_max);

        public WeatherRegisterDtoBuilder WithTemp_min(long temp_min) => Construct(c => c.Main.Temp_min = temp_min);

        public WeatherRegisterDtoBuilder WithHumidity(long humidity) => Construct(c => c.Main.Humidity = humidity);
    }
}
