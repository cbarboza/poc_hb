﻿using FluentAssertions;
using MicroService1.Domain.Service.Dtos;
using MicroService1.Validators;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Poc_Hb.Test.Domain.Validations
{
    public class RegisterDtoValidatorTest
    {
        [Fact]
        public void Should_Return_Dto_As_Valid()
        {
            var dto = new RegisterDto
            {
                CityName = "Blumenau",
                Country = "BR"
            };

            var validation = new RegisterValidator();
            var result = validation.Validate(dto);

            result.Errors.Should().HaveCount(0);
        }

        [Fact]
        public void Should_Return_Dto_As_Invalid()
        {
            var dto = new RegisterDto();

            var validation = new RegisterValidator();
            var result = validation.Validate(dto);

            var expected = new List<string>()
            {
                "CityName", "Country", "Country"
            };
            
            result.Errors.Should().HaveCount(expected.Count);
            result.Errors.Select(s => s.PropertyName).ToList().Should().BeEquivalentTo(expected);
        }
    }
}