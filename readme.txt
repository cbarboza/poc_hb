Considerações para o desenvolvimento da aplicação:

A arquitetura seria baseada em micro serviços e workers, possibilitando assim melhor distribuição e escalabilidade, 
com base na necessidade de processamento e disponibilidade.

A linguagem seria C#, utilizando .NETCore 3.1, possibilitando assim a utilização de Docker com Swarm / Kubernetes e servidores 
Linux para a distribuição das aplicações.

Para servidores Windows, a aplicação poderia ser instalada como serviço, por meio de Self Host, não havendo necessidade 
de IIS (windows nano server).

Para a flexibilidade do desenvolvimento das integrações:

	Definição de contratos para a manipulação das integrações.

	Os contratos seriam versionados e disponibilizados, por exemplo, via Nuget.

Comunicação dos micro-serviços por meio de fila (RabbitMQ, ZeroMQ…), utilizando uma camada de abstração (MassTransit, RawRabbit...) 
para a configuração do comportamento e tratamento das mensagens e o padrão de projetos Pub/Sub, para o transporte.

O processamento das mensagens recebidas seria feito por meio de Workers.

Cada Worker teria um conjunto de processors (assíncronos), responsáveis por cada tipo de mensagem recebida.

Seria utilizado um framework de resiliência e manipulação de falhas transientes, para a comunicação entre as aplicações, 
fila e banco de dados (Ex.: Polly).

Para lidar com possíveis falhas (conhecidas e desconhecidas), poderíamos utilizar um framework de mercado para o tracking das 
exceções (Por exemplo, Sentry, utilizando o Webhook com slack para um canal de suporte)

Poderíamos utilizar SAGA para garantir todo o fluxo, notificando as partes interessadas caso uma falha viesse a acontecer

Poderia ser elaborado um meio de comunicação com o uso de email, SMS ou o uso por exemplo, de uma plataforma de resposta a incidentes
SaaS (PagerDutty) para a equipe de suporte, além do log sensível a cada contexto.

Poderia ser utilizado o ELK com Kibana, para a centralização dos logs de cada aplicação.

O Registro de cada ação poderia ser feita utilizando o padrão Event Sourcing, para a possibilidade de um tracking das informações persistidas 
em uma linha de tempo.



A aplicação:

A arquitetura proposta para a construção dos micro-serviços foi a Onion, por se tratar de uma arquitetura simples e de fácil desenvolvimento.

Foi utilizado injeção de dependência para a modularização das integrações, facilitando também a criação de 
testes unitários, integração, aceitação e stress.

A persistência é feita em um banco de dados relacional (Postgres).

A comunicação com banco foi feita utilizando Entity Framework, com o padrão Repository.

A exposição das API’s foi feita por meio do Swagger, para facilitar o uso dos endpoints.

Para segurança das API’s foi implementado um middleware para autenticação por token (Bearer JWT).

Para o Log, foi utilizado o Serilog pela sua flexibilidade (Sinks para console, arquivo, ELK, etc...)
Como exemplo, os micro serviços logan no console e geram arquivos de log na pasta Log.


A Aplicação Exemplo:

O projeto foi feito utilizando .NETCore 3.1 e banco de dados Postgres.

O Worker com seus processors estão na camada de mensageria para ilustrar a sua funcionalidade.

No mundo real, os Workers seriam serviços separados para oferecer escalabilidade separada.

Fluxo:

	O micro-serviço 1 publica uma requisição para recuperar as informações sobre o tempo de uma determinada cidade / pais

	O Worker responsável irá processar a mensagem e executar um request no micro-serviço 2.

	O micro-serviço 2 irá requisitar as informações utilizando a API de tempo, processar as informações e publicar uma mensagem.

	O Worker responsável irá processar a mensagem e executar um request no micro-serviço 1;

	O micro-serviço 1 irá salvar as informações sobre o tempo no banco de dados.


Pré requisitos:

	Visual Studio 2019 / VsCode
	.NETCore 3.1
	Docker
	Docker-Compose

Para executar a aplicação:

Baixe o projeto para o seu computador

Nos arquivos appsettings.json (MicroService01, MicroService02 e MessageBroker), modifique o IP pelo IP docker-machine da máquina

Execute:

	- docker-compose build
	
	-> Esse comando irá preparar o ambiente e criar os container utilizados no exemplo:
	
		- Postgres (banco postgres, usuário postgres e senha postgres)
		- RabbitMQ
		- MicroService01
		- MicroService02
		- MessageBroker.Worker
		- Definiçao da rede interna
		- Criação de um volume para o uso do postgres (uma vez removido o container, os dados permanecerão no volume). 

	- docker-compose up
	
	-> Esse comando irá executar a stack, subindo os 5 containers

Utilizei a abordagem Code First para a criação das entidades e fiz o setup para a aplicação garantir que as entidades existam no banco

Abra um browser da sua preferência e utilize a url: <Seu Ip Docker>:5001/swagger/ 

Para ter acesso aos endpoints da aplicação, clique no Botão Authorize e informe o token Jwt abaixo:

Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOiIyMDIwIiwiVXNlck5hbWUiOiJDYXJsb3MgRWR1YXJkbyIsImN0ciI6InB0LUJSIiwiaHN0IjoiMzUuMTkwLjE2NC44NyIsInR6aSI6IkFtZXJpY2EvU2FvX1BhdWxvIiwiaWNyIjoiRmFsc2UiLCJjd3QiOiIiLCJuYmYiOjE1Nzg0MTY5NDQsImV4cCI6MTU5OTkzMTM0NCwiaWF0IjoxNTcwMDE2OTQ0LCJpc3MiOiJUZXN0ZSIsImF1ZCI6IlRlc3RlIn0.pE9oEQqXKVkzm03RRWi2e3skwLjWSNrHoR1-eZckFWY

Utilize o endpoint para registrar uma cidade (Post/register), informando a cidade e o pais.
Ex.:
{
"cityName": "blumenau",
"country": "br"
}

Utilize o endpoint para listar as cidades cadastradas (Get)

Para parar a execução, utilize Ctrl+C

Para remover os containers:

	- docker-compose down
	-> Esse comando irá remover os containers e a rede.	


	
Referências:

http://masstransit-project.com/

https://github.com/pardahlman/RawRabbit

https://github.com/App-vNext/Polly

https://martinfowler.com/eaaDev/EventSourcing.html

https://martinfowler.com/eaaCatalog/

https://www.postgresql.org/

https://www.rabbitmq.com/

https://docs.microsoft.com/en-us/dotnet/core/

https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/linux-nginx?tabs=aspnetcore2x&view=aspnetcore-3.1

https://docs.microsoft.com/en-us/aspnet/core/fundamentals/servers/?tabs=windows&view=aspnetcore-3.1

https://docs.microsoft.com/en-us/nuget/what-is-nuget

https://docs.docker.com/engine/swarm/

https://docs.microsoft.com/en-us/aspnet/web-api/overview/hosting-aspnet-web-api/use-owin-to-self-host-web-api

https://docs.microsoft.com/en-us/windows-server/get-started/getting-started-with-nano-server

https://msdn.microsoft.com/en-us/magazine/ee291514.aspx

https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern

https://en.wikipedia.org/wiki/SOLID_(object-oriented_design)

https://www.pagerduty.com/

https://swagger.io/

https://serilog.net/

https://jwt.io/
