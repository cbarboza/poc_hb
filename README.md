# poc_hb

  

**A Aplicação Exemplo:**

  

O projeto foi feito utilizando .NETCore 3.1 e banco de dados Postgres.

  

O Worker com seus processors estão na camada de mensageria para ilustrar a sua funcionalidade.

Na pasta common, estão os componentes utilizados pelos demais projetos, que poderiam ser transformados em pacotes (Nuget):

    - Helpers - para httpclient e retentativa
    - MessageBroker - para uma abstração utilizando RabbitMQ
    - Security - para o Token Jwt

  

No mundo real, os Workers seriam serviços separados para oferecer escalabilidade separada.

  

**Fluxo:**

  

O *micro-serviço 1* publica uma requisição para recuperar as informações sobre o tempo de uma determinada cidade / pais

  

O *Worker* responsável irá processar a mensagem e executar um request no *micro-serviço 2*.

  

O *micro-serviço 2* irá requisitar as informações utilizando a API de tempo, processar as informações e publicar uma mensagem.

  

O *Worker* responsável irá processar a mensagem e executar um request no *micro-serviço 1*;

  

O *micro-serviço 1* irá salvar as informações sobre o tempo no banco de dados.

  
  

**Pré requisitos:**

  

	Visual Studio 2019 / VsCode

	.NETCore 3.1

	Docker

	Docker-Compose

  

**Para executar a aplicação:**

  

Baixe o projeto para o seu computador

  

Nos arquivos appsettings.json (*MicroService01, MicroService02 e MessageBroker*), modifique o IP pelo IP docker-machine da máquina

  

**Execute:**

  

`docker-compose build`

Esse comando irá preparar o ambiente e criar os container utilizados no exemplo:

	- Postgres (banco, usuário e senha utilizam a palavra: postgres)

	- RabbitMQ

	- MicroService01

	- MicroService02

	- MessageBroker.Worker

	- Definiçao da rede interna

	- Criação de um volume para o uso do postgres 
	(uma vez removido o container, os dados permanecerão no volume).

  

`docker-compose up`

Esse comando irá executar a stack, subindo os 5 containers

  

Utilizei a abordagem Code First para a criação das entidades e fiz o setup para a aplicação garantir que as entidades existam no banco

  

Abra um browser da sua preferência e utilize a url: `Seu_Ip_Docker:5001/swagger/`

  

Para ter acesso aos endpoints da aplicação, clique no Botão Authorize e informe o token Jwt abaixo:

  

    Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOiIyMDIwIiwiVXNlck5hbWUiOiJDYXJsb3MgRWR1YXJkbyIsImN0ciI6InB0LUJSIiwiaHN0IjoiMzUuMTkwLjE2NC44NyIsInR6aSI6IkFtZXJpY2EvU2FvX1BhdWxvIiwiaWNyIjoiRmFsc2UiLCJjd3QiOiIiLCJuYmYiOjE1Nzg0MTY5NDQsImV4cCI6MTU5OTkzMTM0NCwiaWF0IjoxNTcwMDE2OTQ0LCJpc3MiOiJUZXN0ZSIsImF1ZCI6IlRlc3RlIn0.pE9oEQqXKVkzm03RRWi2e3skwLjWSNrHoR1-eZckFWY

  
  

Utilize o endpoint para registrar uma cidade (Post/register), informando a cidade e o pais.
Ex.:

{
  "cityName": "blumenau",
  "country": "br"
}


Utilize o endpoint para listar as cidades cadastradas (Get)

  
Para parar a execução, utilize Ctrl+C  

Para remover os containers:

  

`docker-compose down`

Esse comando irá remover os containers e a rede.
