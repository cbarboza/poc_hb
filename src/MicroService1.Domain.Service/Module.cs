﻿using Infrastructure.MessageBroker.Interfaces;
using Infrastructure.MessageBroker.Rabbit;
using MicroService1.Domain.Service.Interfaces;
using MicroService1.Domain.Service.Services;
using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class Module
    {
        public static void RegisterDomainService(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddScoped<IWeatherService, WeatherService>();
            services.AddScoped<IMessageBroker, RabbitMqBroker>();

            MicroService1.Infrastructure.Database.Module.RegisterDatabase(services, configuration.GetConnectionString("PGConnection"));
        }
    }
}