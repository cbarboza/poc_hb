﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;

namespace MicroService1.Domain.Service
{
    public static class Migration
    {
        public static void MigrateDb(IApplicationBuilder app, ILogger logger)
        {
            Infrastructure.Database.Module.MigrateDb(app, logger);
        }
    }
}
