﻿using MicroService1.Domain.Service.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MicroService1.Domain.Service.Interfaces
{
    public interface IWeatherService
    {
        Task<IEnumerable<WeatherDto>> GetWeathers();

        Task SaveWeather(WeatherRegisterDto dto);

        void Register(RegisterDto dto);
    }
}