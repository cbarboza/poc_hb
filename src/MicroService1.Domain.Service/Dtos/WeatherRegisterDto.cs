﻿using MicroService1.Domain.Entities;

namespace MicroService1.Domain.Service.Dtos
{
    public class WeatherRegisterDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public MainDto Main { get; set; }

        public SysDto Sys { get; set; }

        public static Weather AsEntity(WeatherRegisterDto dto)
        {
            return new Weather
            {
                Id = dto.Id,
                CityName = dto.Name,
                Country = dto.Sys.Country,
                FeelsLike = dto.Main.Feels_Like,
                Humidity = dto.Main.Humidity,
                Maximum = dto.Main.Temp_max,
                Minimum = dto.Main.Temp_min,
                Pressure = dto.Main.Pressure,
                Temperature = dto.Main.Temp
            };
        }

        public static Weather ToUpdte(Weather entity, WeatherRegisterDto dto)
        {
            entity.CityName = dto.Name;
            entity.Country = dto.Sys.Country;
            entity.FeelsLike = dto.Main.Feels_Like;
            entity.Humidity = dto.Main.Humidity;
            entity.Maximum = dto.Main.Temp_max;
            entity.Minimum = dto.Main.Temp_min;
            entity.Pressure = dto.Main.Pressure;
            entity.Temperature = dto.Main.Temp;

            return entity;
        }
    }
}