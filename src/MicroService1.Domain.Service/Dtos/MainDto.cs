﻿namespace MicroService1.Domain.Service.Dtos
{
    public class MainDto
    {
        public float Temp { get; set; }
        public float Feels_Like { get; set; }
        public float Temp_min { get; set; }
        public float Temp_max { get; set; }
        public float Pressure { get; set; }
        public float Humidity { get; set; }
    }
}