﻿using System;
using System.Linq.Expressions;

namespace MicroService1.Domain.Service.Dtos
{
    public class WeatherDto
    {
        public string CityName { get; set; }
        public string Country { get; set; }

        public float Temperature { get; set; }
        public float Minimum { get; set; }
        public float Maximum { get; set; }
        public float FeelsLike { get; set; }
        public float Pressure { get; set; }
        public float Humidity { get; set; }

        public WeatherDto()
        { }


        public WeatherDto(Entities.Weather weather)
        {
            CityName = weather.CityName;
            Country = weather.Country;
            Temperature = weather.Temperature;
            Minimum = weather.Minimum;
            Maximum = weather.Maximum;
            FeelsLike = weather.FeelsLike;
            Pressure = weather.Pressure;
            Humidity = weather.Humidity;
        }

        public static Expression<Func<Entities.Weather, WeatherDto>> Projection => weather => new WeatherDto(weather);
    }
}