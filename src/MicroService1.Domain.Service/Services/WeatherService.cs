﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using MicroService1.Domain.Service.Interfaces;
using MicroService1.Domain.Interfaces.Repositories;
using Infrastructure.MessageBroker.Interfaces;
using Microsoft.Extensions.Configuration;
using MicroService1.Domain.Service.Dtos;
using MicroService1.Domain.Entities;

namespace MicroService1.Domain.Service.Services
{
    public class WeatherService : IWeatherService
    {
        private readonly IWeatherRepository _weatherRepository;
        private readonly IMessageBroker _messageBroker;
        private readonly IConfiguration _configuration;

        public WeatherService(IWeatherRepository weatherRepository, IMessageBroker messageBroker, IConfiguration configuration)
        {
            _weatherRepository = weatherRepository;
            _messageBroker = messageBroker;
            _configuration = configuration;
        }

        public async Task<IEnumerable<WeatherDto>> GetWeathers() => await _weatherRepository.GetAll().Select(WeatherDto.Projection).ToListAsync().ConfigureAwait(false);

        public async Task SaveWeather(WeatherRegisterDto dto)
        {
            // Poderíamos utilizar uma extensão para executar um UpSert pelo EF
            var entity = _weatherRepository.Get(g => g.Id == dto.Id).FirstOrDefault();
            if (entity == null)
            {
                _weatherRepository.Add(WeatherRegisterDto.AsEntity(dto));
            }
            else
            {                
                _weatherRepository.Edit(WeatherRegisterDto.ToUpdte(entity, dto));

            }

            await _weatherRepository.CommitAsync().ConfigureAwait(false);
        }


        public void Register(RegisterDto dto)
        {
            _messageBroker.Send(dto, _configuration.GetSection("Queue")["Exchange"]);
        }
    }
}