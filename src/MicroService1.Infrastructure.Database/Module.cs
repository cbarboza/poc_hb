﻿using Helpers;
using MicroService1.Domain.Interfaces.Repositories;
using MicroService1.Infrastructure.Database.Context;
using MicroService1.Infrastructure.Database.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace MicroService1.Infrastructure.Database
{
    public static class Module
    {
        public static void RegisterDatabase(IServiceCollection services, string connectionString)
        {
            services.AddScoped<IWeatherRepository, WeatherRepository>();

            services.AddDbContext<DomainContext>(options => options.UseNpgsql(connectionString, x => x.MigrationsAssembly("MicroService1")));
        }

        public static void MigrateDb(IApplicationBuilder app, ILogger logger)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<DomainContext>();


                RetryHelper.RetryOnException(() => context.Database.Migrate(), logger);
            }
        }
    }
}
