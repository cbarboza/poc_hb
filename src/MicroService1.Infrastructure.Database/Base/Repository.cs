﻿using MicroService1.Domain.Entities.Base;
using MicroService1.Domain.Interfaces.Base;
using MicroService1.Infrastructure.Database.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MicroService1.Infrastructure.Database.Base
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DomainContext DomainContext;

        public Repository(DomainContext context)
        {
            DomainContext = context;
        }

        public void Add(TEntity entity)
        {
            DomainContext.Set<TEntity>().Add(entity);
        }     

        public void Commit()
        {
            DomainContext.SaveChanges();
        }

        public async Task CommitAsync()
        {
            await DomainContext.SaveChangesAsync();
        }     

        public void Delete(Func<TEntity, bool> predicate)
        {
            DomainContext.Set<TEntity>()
                .Where(predicate).ToList()
                .ForEach(del => DomainContext.Set<TEntity>().Remove(del));
        }

        public void Edit(TEntity entity)
        {
            DomainContext.Entry(entity).State = EntityState.Modified;
        }

        public async Task<TEntity> FindAsync(params object[] key)
        {
            return await DomainContext.Set<TEntity>().FindAsync(key).ConfigureAwait(false);
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> whereCondition)
        {
            return DomainContext
                .Set<TEntity>()
                .Where(whereCondition);
        }

        public virtual async Task<T> GetByIdAsync<T, TKeyType>(TKeyType id) where T : BaseEntity<TKeyType>
        {
            return await DomainContext
                .Set<T>()
                .Where(p => p.Id.Equals(id))
                .FirstOrDefaultAsync();
        }

        public IQueryable<TEntity> GetAll() => DomainContext.Set<TEntity>();


        bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                DomainContext.Dispose();
            }

            _disposed = true;
        }

    }
}
