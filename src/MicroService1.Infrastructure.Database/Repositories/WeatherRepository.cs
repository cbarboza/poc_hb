﻿using MicroService1.Domain.Interfaces.Repositories;
using MicroService1.Infrastructure.Database.Base;
using MicroService1.Infrastructure.Database.Context;

namespace MicroService1.Infrastructure.Database.Repositories
{
    public class WeatherRepository : Repository<Domain.Entities.Weather>, IWeatherRepository
    {
        public WeatherRepository(DomainContext context) : base(context)
        { }

    }
}