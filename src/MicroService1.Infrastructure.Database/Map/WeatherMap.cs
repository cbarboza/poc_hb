﻿using MicroService1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MicroService1.Infrastructure.Database.Map
{
    public class WeatherMap : IEntityTypeConfiguration<Weather>
    {
        public void Configure(EntityTypeBuilder<Weather> builder)
        {
            builder.ToTable(nameof(Weather));

            builder.HasKey(t => t.Id);

            builder.Property(t => t.CityName).IsRequired();
            builder.Property(t => t.Country).IsRequired();

            builder.Property(t => t.Temperature);
            builder.Property(t => t.Minimum);
            builder.Property(t => t.Maximum);
            builder.Property(t => t.FeelsLike);
            builder.Property(t => t.Pressure);
            builder.Property(t => t.Humidity);
        }
    }
}