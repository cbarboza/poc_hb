﻿using Microsoft.EntityFrameworkCore;

namespace MicroService1.Infrastructure.Database.Context
{
    public class DomainContext : DbContext
    {
        public DomainContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasPostgresExtension("uuid-ossp");

            builder.ApplyConfigurationsFromAssembly(typeof(DomainContext).Assembly);
        }
    }
}