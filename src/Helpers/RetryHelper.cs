﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Helpers
{    
    public static class RetryHelper
    {
        public static void RetryOnException(Action operation, ILogger logger, int times = 5, int delayInSeconds = 10)
        {
            var delay = TimeSpan.FromSeconds(delayInSeconds);
            var attempts = 0;
            do
            {
                try
                {
                    operation();
                    break; 
                }
                catch 
                {
                    attempts++;
                    logger.LogInformation($"Exception captured: retying {attempts} from {times}");
                    if (attempts == times)
                        throw;

                    Task.Delay(delay).Wait();
                }
            } while (true);
        }
    }
}
