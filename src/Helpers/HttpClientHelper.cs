﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public class HttpClientHelper : IHttpClientHelper
    {
        public async Task<HttpStatusCode> PostAsync(Uri uri, string jwtToken, string requestUri, object payload)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                SetClient(uri, jwtToken, httpClient);

                HttpResponseMessage response;
                try
                {
                    var jsonContent = JsonConvert.SerializeObject(payload);
                    var contentString = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                    response = await httpClient.PostAsync(requestUri, contentString).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Ocorreu um erro ao tentar executar o serviço {uri.Host}", ex);
                }
                return response.StatusCode;
            }
        }

        public async Task<HttpResponseMessage> GetAsync(Uri uri, string requestUri)
        {
            return await GetAsync(uri, string.Empty, requestUri).ConfigureAwait(false);
        }

        public async Task<HttpResponseMessage> GetAsync(Uri uri, string jwtToken, string requestUri)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                SetClient(uri, jwtToken, httpClient);

                HttpResponseMessage result = await httpClient.GetAsync(requestUri).ConfigureAwait(false);

                return result;
            }
        }

        private static void SetClient(Uri uri, string token, HttpClient httpClient)
        {
            httpClient.BaseAddress = uri;
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (!string.IsNullOrEmpty(token))
                httpClient.DefaultRequestHeaders.Add("Authorization", token);
        }
    }
}
