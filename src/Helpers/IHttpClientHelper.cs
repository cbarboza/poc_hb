﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Helpers
{
    public interface IHttpClientHelper
    {
        Task<HttpResponseMessage> GetAsync(Uri uri, string requestUri);
        Task<HttpResponseMessage> GetAsync(Uri uri, string jwtToken, string requestUri);

        Task<HttpStatusCode> PostAsync(Uri uri, string jwtToken, string requestUri, object payload);
    }
}
