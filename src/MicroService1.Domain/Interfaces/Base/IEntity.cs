﻿namespace MicroService1.Domain.Interfaces.Base
{
    public interface IEntity<TKeyType>
    {
        TKeyType Id { get; set; }
    }
}
