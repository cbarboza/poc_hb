﻿namespace MicroService1.Domain.Interfaces.Base
{
    public interface IBaseEntity<TKeyType> : IEntity<TKeyType>
    {
    }
}
