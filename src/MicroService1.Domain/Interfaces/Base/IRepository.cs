﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MicroService1.Domain.Interfaces.Base
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        void Add(TEntity entity);

        void Commit();

        Task CommitAsync();

        void Delete(Func<TEntity, bool> predicate);

        void Edit(TEntity entity);

        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> where);

        IQueryable<TEntity> GetAll();
    }
}
