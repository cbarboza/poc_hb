﻿
using MicroService1.Domain.Entities;
using MicroService1.Domain.Interfaces.Base;

namespace MicroService1.Domain.Interfaces.Repositories
{
    public interface IWeatherRepository : IRepository<Weather>
    {
    }
}
