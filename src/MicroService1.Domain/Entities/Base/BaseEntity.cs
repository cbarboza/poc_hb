﻿
using MicroService1.Domain.Interfaces.Base;

namespace MicroService1.Domain.Entities.Base
{
    public abstract class BaseEntity<TKeyType> : IBaseEntity<TKeyType>
    {
        public TKeyType Id { get; set; }
        
    }
}
