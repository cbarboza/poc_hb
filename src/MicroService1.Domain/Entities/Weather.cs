﻿using MicroService1.Domain.Entities.Base;

namespace MicroService1.Domain.Entities
{
    public class Weather : BaseEntity<long>
    {
        public string CityName { get; set; }
        public string Country { get; set; }

        public float Temperature { get; set; }
        public float Minimum { get; set; }
        public float Maximum { get; set; }
        public float FeelsLike { get; set; }
        public float Pressure { get; set; }
        public float Humidity { get; set; }

    }
}
