﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Infrastructure.MessageBroker.Rabbit;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using MessageBroker.Worker.BackgroundServices.Domain;
using Helpers;

namespace MessageBroker.Worker.BackgroundServices.Workers
{
    public class ReceiveWorker2 : BaseRabbitMqWorker
    {
        public ReceiveWorker2(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpClientHelper httpClientHelper) : base(configuration, loggerFactory, httpClientHelper)
        {
            _logger.LogInformation("Receive 2...");
            InitRabbitMQ(_configuration.GetSection("Queues")["Receive2"]);
        }

        protected override async Task HandleMessage(string content)
        {
            _logger.LogInformation("Handle Message...");

            var payload = JsonConvert.DeserializeObject<RegisterDto>(content);

            var uri = new Uri(_configuration.GetSection("Endpoints")["Uri2"]);

            _logger.LogInformation($"Post payload to {_configuration.GetSection("Endpoints")["Uri1"]}");
            
            await _httpClientHelper.PostAsync(uri, _jwt, string.Empty, payload).ConfigureAwait(false);
            _logger.LogInformation($"Received (02) >>> {content}");
        }
    }
}