﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Infrastructure.MessageBroker.Rabbit;
using System;
using System.Threading.Tasks;
using MessageBroker.Worker.BackgroundServices.Domain;
using Newtonsoft.Json;
using Helpers;

namespace MessageBroker.Worker.BackgroundServices.Workers
{
    public class ReceiveWorker1 : BaseRabbitMqWorker
    {
        public ReceiveWorker1(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpClientHelper httpClientHelper) : base(configuration, loggerFactory, httpClientHelper)
        {
            _logger.LogInformation("Receive 1...");
            InitRabbitMQ(_configuration.GetSection("Queues")["Receive1"]);
        }

        protected override async Task HandleMessage(string content)
        {
            _logger.LogInformation("Handle Message...");

            var payload = JsonConvert.DeserializeObject<WeatherRegisterDto>(content);

            var uri = new Uri(_configuration.GetSection("Endpoints")["Uri1"]);

            _logger.LogInformation($"Post payload to {_configuration.GetSection("Endpoints")["Uri1"]}");

            await _httpClientHelper.PostAsync(uri, _jwt, string.Empty, payload).ConfigureAwait(false);
            _logger.LogInformation($"Received (01) >>> {content}");
        }
    }
}