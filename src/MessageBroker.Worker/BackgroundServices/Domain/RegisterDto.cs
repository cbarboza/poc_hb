﻿namespace MessageBroker.Worker.BackgroundServices.Domain
{
    internal class RegisterDto
    {
        public string CityName { get; set; }
        public string Country { get; set; }
    }
}