﻿using Infrastructure.MessageBroker.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

namespace Infrastructure.MessageBroker.Rabbit
{
    public class RabbitMqBroker : IMessageBroker
    {
        private readonly IConfiguration _configuration;
        protected readonly ILogger _logger;

        public RabbitMqBroker(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _configuration = configuration;
            _logger = loggerFactory.CreateLogger<RabbitMqBroker>();
        }

        public void Send(object message, string exchange)
        {
            var connection = RabbitHelper.CreateConnection(_configuration.GetSection("RabbitMQ"), _logger);

            using (var channel = connection.CreateModel())
            {
                channel.BasicPublish(exchange: exchange,
                                     routingKey: "",
                                     basicProperties: null,
                                     body: EncodeMessage(SerializeMessage(message)));
            }

            connection.Dispose();
        }

        private static byte[] EncodeMessage(string messageSerialized)
        {
            return Encoding.UTF8.GetBytes(messageSerialized);
        }

        private static string SerializeMessage(object message)
        {
            return JsonConvert.SerializeObject(message, RabbitHelper.SerializerSettings());
        }
    }
}