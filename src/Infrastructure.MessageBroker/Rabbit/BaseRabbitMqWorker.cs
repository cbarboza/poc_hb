﻿using Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.MessageBroker.Rabbit
{
    public abstract class BaseRabbitMqWorker : BackgroundService
    {
        protected readonly ILogger _logger;
        protected IConnection _connection;
        protected IModel _channel;
        protected readonly IConfiguration _configuration;
        protected string _queue;
        protected readonly string _jwt;
        protected readonly IHttpClientHelper _httpClientHelper;

        public BaseRabbitMqWorker(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpClientHelper httpClientHelper)
        {
            _configuration = configuration;
            _logger = loggerFactory.CreateLogger<BaseRabbitMqWorker>();

            _jwt = _configuration.GetSection("Security")["Jwt"];

            _httpClientHelper = httpClientHelper;
        }

        protected void InitRabbitMQ(string queue)
        {
            _queue = queue;

            _logger.LogInformation("Starting...");

            _connection = RabbitHelper.CreateConnection(_configuration.GetSection("RabbitMQ"), _logger);

            _channel = _connection.CreateModel();

            _channel.ExchangeDeclare($"{_queue}.exchange", ExchangeType.Topic);
            _channel.QueueDeclare(_queue, true, false, false, null);
            _channel.QueueBind(_queue, $"{_queue}.exchange", "", null);
            _channel.BasicQos(0, 1, false);

            _connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
        }

        protected override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var consumer = new AsyncEventingBasicConsumer(_channel);

            consumer.Received += async (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body);

                await HandleMessage(content).ConfigureAwait(false);
                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _connection.ConnectionShutdown += (_, e) => _logger.LogError("RabbitMQ connection shutdown.");
            _connection.RecoverySucceeded += (_, e) => _logger.LogInformation("RabbitMQ recovery succeeded.");

            _channel.BasicConsume(_queue, false, consumer);
            return Task.CompletedTask;
        }

        protected abstract Task HandleMessage(string content);

        private void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e) { }
        private void OnConsumerUnregistered(object sender, ConsumerEventArgs e) { }
        private void OnConsumerRegistered(object sender, ConsumerEventArgs e) { }
        private void OnConsumerShutdown(object sender, ShutdownEventArgs e) { }
        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e) { }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }
    }
}