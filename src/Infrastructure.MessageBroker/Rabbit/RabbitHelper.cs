﻿using Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RabbitMQ.Client;
using System;

namespace Infrastructure.MessageBroker.Rabbit
{
    public static class RabbitHelper
    {
        public static IConnection CreateConnection(IConfigurationSection section, ILogger logger)
        {
            IConnection connection = null;

            RetryHelper.RetryOnException(() => connection = GetConnection(section).CreateConnection(), logger);

            return connection;
        }

        private static ConnectionFactory GetConnection(IConfigurationSection section)
        {
            return new ConnectionFactory()
            {
                HostName = section["Server"],
                UserName = section["User"],
                Password = section["Pass"],
                RequestedHeartbeat = 60,
                DispatchConsumersAsync = true,
                AutomaticRecoveryEnabled = true,
                TopologyRecoveryEnabled = true,
                NetworkRecoveryInterval = TimeSpan.FromSeconds(5),
                UseBackgroundThreadsForIO = false
            };
        }

        public static JsonSerializerSettings SerializerSettings()
        {
            return new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
        }

    }
}
