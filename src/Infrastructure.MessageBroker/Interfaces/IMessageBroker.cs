﻿namespace Infrastructure.MessageBroker.Interfaces
{
    public interface IMessageBroker
    {
        void Send(object message, string exchange);
    }
}
