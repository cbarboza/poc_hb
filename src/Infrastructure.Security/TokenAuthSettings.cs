﻿namespace Infrastructure.Security
{
    public class TokenAuthSettings
    {
        public string Key { get; set; }
        public string SiteUrl { get; set; }
        public string TokenCreationPath { get; set; }
        public string Audience { get; set; }
        public string Issuer { get; set; }
    }
}
