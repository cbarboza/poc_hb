﻿using System.Globalization;

namespace PocCore.Domain.Resources
{
    public static class GetStringFromResources
    {

        public static string Get(string key)
        {
            return Default.ResourceManager.GetString(key, CultureInfo.CurrentCulture);
        }

    }
}