﻿namespace PocCore.Domain.Interfaces.Base
{
    public interface IBaseEntity<TKeyType> : IEntity<TKeyType>
    {
    }
}
