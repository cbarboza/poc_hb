namespace PocCore.Domain.Interfaces.Base
{
    public interface IDeletable
    {
        bool IsDeleted { get; set; }

        void Delete();
    }
}