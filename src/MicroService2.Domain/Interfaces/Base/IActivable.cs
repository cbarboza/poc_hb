﻿namespace PocCore.Domain.Interfaces.Base
{
    public interface IActivable
    {
        bool IsActive { get; set; }
    }
}
