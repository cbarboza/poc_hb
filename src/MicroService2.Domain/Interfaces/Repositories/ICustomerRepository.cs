﻿using PocCore.Domain.Entities;
using PocCore.Domain.Interfaces.Base;

namespace PocCore.Domain.Interfaces.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}
