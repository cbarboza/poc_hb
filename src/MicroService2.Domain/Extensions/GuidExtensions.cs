﻿using System;

namespace PocCore.Domain.Extensions
{
    public static class GuidExtensions
    {
        public static string ToHashString(this Guid guid)
        {
            return guid.ToString().Replace("-", string.Empty).ToUpper();
        }
    }
}
