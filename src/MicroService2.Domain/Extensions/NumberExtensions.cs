﻿using System;

namespace PocCore.Domain.Extensions
{
    public static class NumberExtensions
    {

        public static float WithPrecision(this float value, int digits = 2)
        {
            return (float)Math.Round(float.IsNaN(value) ? 0 : value, digits);
        }
    }
}