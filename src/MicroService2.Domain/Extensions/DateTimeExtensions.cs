﻿using System;

namespace PocCore.Domain.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime WithMaxTime(this DateTime date) => date.Date.AddDays(1).AddMilliseconds(-1);
    }
}
