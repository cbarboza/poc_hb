﻿namespace PocCore.Domain.Extensions
{
    public static class StringExtensions
    {
        public static string ToValidFileName(this string source)
        {
            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
            {
                source = source.Replace(c, '_');
            }
            // Remove a vírgula para evitar erro de download
            source = source.Replace(",", ".");
            return source.RemoveAccent();
        }

        public static string RemoveAccent(this string txt)
        {
            if (txt == null) return null;
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        public static string Truncate(this string value, int maxChars)
        {
            return value == null
                ? null
                : value.Length <= maxChars ? value : value.Substring(0, maxChars);
        }

    }
}
