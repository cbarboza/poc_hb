﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace PocCore.Domain.Extensions
{
    public static class EnumExtensions
    {
        public static TAttribute GetAttribute<TAttribute>(this Enum value) where TAttribute : Attribute
        {
            var type = value.GetType();
            var members = type.GetMember(value.ToString());
            var attributes = members[0].GetCustomAttributes(typeof(TAttribute)).ToList();
            return (attributes.Count > 0) ? (TAttribute) attributes[0] : null;
        }

        public static string GetDescription(this Enum value)
        {
            if (value != null)
            {
                Type type = value.GetType();
                string name = Enum.GetName(type, value);
                if (name != null)
                {
                    FieldInfo field = type.GetField(name);
                    if (field != null)
                    {
                        DescriptionAttribute attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                        if (attr != null)
                        {
                            return attr.Description;
                        }
                    }
                }
            }
            return null;
        }

        public static string GetDescriptionFlags(this Enum value)
        {
            List<string> flags = new List<string>();

            var enumType = value.GetType();
            if (enumType.GetCustomAttributes<FlagsAttribute>().Any())
            {
                foreach (Enum enumValue in Enum.GetValues(enumType))
                {
                    if (value.HasFlag(enumValue))
                        flags.Add(enumValue.GetDescription());
                }
            }

            return string.Join(" + ", flags);
        }
    }
}