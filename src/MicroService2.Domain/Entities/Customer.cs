﻿using PocCore.Domain.Entities.Base;
using System;

namespace PocCore.Domain.Entities
{
    public class Customer : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public DateTime? DateEvent { get; set; }
    }
}
