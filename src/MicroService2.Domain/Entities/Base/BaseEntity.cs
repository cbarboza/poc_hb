﻿using PocCore.Domain.Interfaces.Base;

namespace PocCore.Domain.Entities.Base
{
    public abstract class BaseEntity<TKeyType> : IBaseEntity<TKeyType>
    {
        public TKeyType Id { get; set; }
        
    }
}
