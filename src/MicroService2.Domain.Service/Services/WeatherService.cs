﻿using Infrastructure.MessageBroker.Interfaces;
using MicroService2.Domain.Service.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using Helpers;
using MicroService2.Domain.Service.Dtos;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;

namespace MicroService2.Domain.Service.Services
{
    public class WeatherService : IWeatherService
    {
        private readonly IMessageBroker _messageBroker;
        private readonly IConfiguration _configuration;
        private readonly IHttpClientHelper _httpClientHelper;
        private readonly ILogger _logger;

        public WeatherService(IMessageBroker messageBroker, IConfiguration configuration, IHttpClientHelper httpClientHelper, ILoggerFactory loggerFactory)
        {
            _messageBroker = messageBroker;
            _configuration = configuration;
            _httpClientHelper = httpClientHelper;
            _logger = loggerFactory.CreateLogger<WeatherService>();
        }

        public async Task<bool> CallAsync(RegisterDto dto)
        {
            var uri = new Uri(_configuration.GetSection("Endpoints")["Uri"]);

            var key = _configuration.GetSection("Endpoints")["Key"];

            var requestUri = $"/data/2.5/weather?q={dto.CityName},{dto.Country}&units=metric&appid={key}";

            var responseMesasge = await _httpClientHelper.GetAsync(uri, string.Empty, requestUri).ConfigureAwait(false);

            var content = responseMesasge.Content.ReadAsStringAsync().Result;

            if(content.Contains("404"))
            {
                _logger.LogInformation($"Cidade / Estado não encontrado(s): {dto.CityName} / {dto.Country}");
                return false;
            }

            var result = JsonConvert.DeserializeObject<WeatherRegisterDto>(content);

            _messageBroker.Send(result, _configuration.GetSection("Queue")["Exchange"]);

            return true;
        }
    }
}