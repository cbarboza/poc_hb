﻿namespace MicroService2.Domain.Service.Dtos
{
    public class WeatherRegisterDto
    {
        public MainDto Main { get; set; }

        public SysDto Sys { get; set; }

        public long Id { get; set; }

        public string Name { get; set; }
    }

    public class SysDto
    {
        public string Country { get; set; }
    }

    public class MainDto
    {
        public float Temp { get; set; }
        public float Feels_Like { get; set; }
        public float Temp_min { get; set; }
        public float Temp_max { get; set; }
        public float Pressure { get; set; }
        public float Humidity { get; set; }
    }
}
