﻿using MicroService2.Domain.Service.Dtos;
using System.Threading.Tasks;

namespace MicroService2.Domain.Service.Interfaces
{
    public interface IWeatherService
    {
        Task<bool> CallAsync(RegisterDto dto);
    }
}