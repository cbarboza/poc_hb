﻿using MicroService2.Domain.Service.Interfaces;
using Infrastructure.MessageBroker.Interfaces;
using Infrastructure.MessageBroker.Rabbit;
using MicroService2.Domain.Service.Services;
using Helpers;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class Module
    {
        public static void RegisterDomainService(this IServiceCollection services)
        {

            services.AddScoped<IWeatherService, WeatherService>();

            services.AddScoped<IMessageBroker, RabbitMqBroker>();

            services.AddSingleton<IHttpClientHelper, HttpClientHelper>();
        }
    }
}