﻿namespace MicroService2.Filters
{
    internal class ApiResponse
    {
        public string Message { get; set; }
        public object Data { get; set; }
    }
}