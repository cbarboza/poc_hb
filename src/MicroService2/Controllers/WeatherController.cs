﻿using MicroService2.Domain.Service.Dtos;
using MicroService2.Domain.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace MicroService2.Controllers
{
    [Route("api/v1/weather")]
    [ApiController]
    public class WeatherController : AuthorizedController
    {
        private readonly IWeatherService _weatherService;

        public WeatherController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        /// <summary>
        /// Recupera as informações do tempo de uma determinada cidade para publicar na fila para persistir no banco
        /// </summary>
        /// /// <returns></returns>
        [HttpPost("")]
        [ProducesResponseType(typeof(RegisterDto), 200)]
        public IActionResult PostAsync(RegisterDto dto)
        {
            _weatherService.CallAsync(dto);

            return Accepted();
        }
    }
}