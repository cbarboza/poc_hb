﻿using System;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace MicroService2.Controllers
{
    [Authorize]
    [ApiController]
    public class AuthorizedController : ControllerBase
    {
        private const string UserDataClaimType = "udt";

        protected User CurrentUser()
        {
            var claims = HttpContext.User.Claims.ToArray();
            var userClaim = HttpContext.User.Claims.First(c => c.Type == UserDataClaimType);
            var userBytesJson = Convert.FromBase64String(userClaim.Value);
            var userJson = Encoding.UTF8.GetString(userBytesJson);
            return JsonConvert.DeserializeObject<User>(userJson);
        }
        protected long UserId => CurrentUser().UserId;
        protected string UserName => CurrentUser().UserName;
    }

    public class User
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
    }
}