﻿using FluentValidation;
using MicroService1.Domain.Service.Dtos;

namespace MicroService1.Validators
{
    public class RegisterValidator : AbstractValidator<RegisterDto>
    {
        public RegisterValidator()
        {
            RuleFor(r => r.CityName).NotEmpty().WithMessage("É necessário informar o campo CityName");
            RuleFor(r => r.Country).NotEmpty().WithMessage("É necessário informar o campo Country");
            RuleFor(r => r.Country).NotEmpty().Length(2).WithMessage("O tamanho do campo deve ser 2. Ex.: BR");
        }
    }
}
