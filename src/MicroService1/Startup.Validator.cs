﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace MicroService1
{
    public partial class Startup
    {
        private void ConfigureValidator(IServiceCollection services)
        {
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = (context) =>
                {
                    var errors = context.ModelState.Values
                                        .SelectMany(x => x.Errors.Select(p => p.ErrorMessage))
                                        .OrderBy(o => o)
                                        .ToList();
                    var result = new
                    {
                        Message = "Erros encontrados na validação",
                        Errors = errors
                    };
                    return new BadRequestObjectResult(result);
                };
            });
        }
    }
}
