using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MicroService1
{
    public class CorsSettings
    {
        public string Origins { get; set; }
    }
    
    public partial class Startup
    {
         public void ConfigureCors(IApplicationBuilder app)
         {
             var corsSettings = Configuration.GetSection("CorsSettings").Get<CorsSettings>();
             app.UseCors(opts =>
             {
                 opts
                     .AllowAnyOrigin()
                     .AllowAnyHeader()
                     .AllowAnyMethod();
             });
         }
 
         public void ConfigureServiceCors(IServiceCollection services)
         {
              var corsSettings = Configuration.GetSection("CorsSettings").Get<CorsSettings>();
              services.AddCors(c =>
              {
                  c.AddPolicy("*", opts =>
                  {
                      opts
                          .AllowAnyOrigin()
                          .AllowAnyHeader()
                          .AllowAnyMethod();
                  });
              });
         }   
    }
}