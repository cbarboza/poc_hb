﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using MicroService1.Domain.Service.Interfaces;
using MicroService1.Domain.Service.Dtos;

namespace MicroService1.Controllers
{
    [Route("api/v1/weather")]
    [ApiController]
    public class WeatherController : AuthorizedController
    {
        private readonly IWeatherService _weatherService;

        public WeatherController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        /// <summary>
        /// Recupera a lista de cidades cadastradas com as informações sobre o tempo
        /// </summary>
        /// /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(WeatherDto), 200)]
        public async Task<IEnumerable<WeatherDto>> GetAsync()
        {
            return await _weatherService.GetWeathers().ConfigureAwait(false);
        }

        /// <summary>
        /// Executa uma chamada na API de terceiros para recuperar as informações sobre o tempo de uma determinada cidade
        /// </summary>
        /// /// <returns></returns>
        [HttpPost("/register")]
        [ProducesResponseType(typeof(WeatherDto), 202)]
        public IActionResult PostRegisterAsync(RegisterDto dto)
        {
            _weatherService.Register(dto);

            return Accepted();
        }

        /// <summary>
        /// Salva no banco de dados a cidade com as informações sobre o tempo
        /// </summary>
        /// /// <returns></returns>
        [HttpPost("")]
        [ProducesResponseType(typeof(WeatherRegisterDto), 200)]
        public async Task<IActionResult> PostAsync(WeatherRegisterDto dto)
        {
            await _weatherService.SaveWeather(dto).ConfigureAwait(false);

            return Ok();
        }

    }
}