﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using FluentValidation.AspNetCore;
using Infrastructure.Security;
using MicroService1.Filters;
using MicroService1.Validators;
using Microsoft.Extensions.Logging;

namespace MicroService1
{
    public partial class Startup
    {

        /// <summary>
        /// Configure application startup options 
        /// </summary>
        /// <param name="env"></param>
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        /// <summary>
        /// Configuration provided by environment 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Configure services
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Ref: https://github.com/aspnet/AspNetCore/issues/8302
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.Configure<TokenAuthSettings>(options => Configuration.GetSection("TokenAuthSettings").Bind(options));
            services.AddHttpContextAccessor();

            ConfigureServiceAuth(services);

            ConfigureServiceCors(services);

            services.AddRouting(o =>
            {
                o.LowercaseUrls = true;
            });
            services.AddMvc(config =>
            {
                config.Filters.Add(typeof(ExceptionFilter));
            })
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssembly(typeof(RegisterValidator).Assembly))
                .AddNewtonsoftJson(options => options.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Utc);

            services.AddSingleton(Configuration);
            services.RegisterDomainService(Configuration);

            ConfigureValidator(services);
            ConfigureServicesSwagger(services);
        }

        /// <summary>
        /// Configure application 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSerilogRequestLogging();

            ConfigureCors(app);

            ConfigureJwtTokenAuthentication(app);
            ConfigureSwagger(app);

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/ping", async context => 
                {
                    await context.Response.WriteAsync("pong!");
                });
                endpoints.MapControllers();
            });

            Domain.Service.Migration.MigrateDb(app, loggerFactory.CreateLogger<Startup>());
        }
    }
}
