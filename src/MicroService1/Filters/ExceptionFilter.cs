﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;

namespace MicroService1.Filters
{
    /// <summary>
    /// Filter used do capture all exceptions and logs with serilog
    /// </summary>
    public class ExceptionFilter : IExceptionFilter
    {
        /// <summary>
        /// Captures all exceptions and logs using serilog
        /// </summary>
        /// <param name="context"></param>
        public void OnException(ExceptionContext context)
        {
            var message = context.Exception.Message;

            Log.Error(context.Exception, "{@exception}");

            context.ExceptionHandled = true;

            var response = context.HttpContext.Response;
            response.StatusCode = (int)HttpStatusCode.InternalServerError;
            response.ContentType = "application/json";

            context.Result = new ObjectResult(new ApiResponse {Message = message, Data = null});
        }
    }
}
