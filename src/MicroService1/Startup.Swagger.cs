﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace MicroService1
{
    public partial class Startup
    {
        public class DownloadFileFilter : IOperationFilter
        {
            public void Apply(OpenApiOperation operation, OperationFilterContext context)
            {    
            }
        }

        public class UploadFileFilter : IOperationFilter
        {
            public void Apply(OpenApiOperation operation, OperationFilterContext context)
            {
                var parameterDescriptionList = context
                    .ApiDescription
                    .ParameterDescriptions
                    .Where(x => x.ModelMetadata.ContainerType == typeof(IFormFile))
                    .ToList();
                if (parameterDescriptionList.Count == 0) return;

                var parameters = operation
                    .Parameters
                    .Where(x => parameterDescriptionList.Exists(p => p.Name == x.Name))
                    .ToList();
                if (parameters.Count == 0) return;

                foreach (var parameter in parameters)
                {
                    operation.Parameters.Remove(parameter);
                }

                var required = parameters.FirstOrDefault()?.Required ?? false;
                operation.Parameters.Add(new OpenApiParameter()
                {
                    Name = "attachment",
                    In = ParameterLocation.Query,
                    Description = "Upload File.",
                    Required = required
                });
            }
        }

        public class UploadFileListFilter : IOperationFilter
        {
            public void Apply(OpenApiOperation operation, OperationFilterContext context)
            {
                var parameterDescriptionList = context
                    .ApiDescription
                    .ParameterDescriptions
                    .Where(x => x.ModelMetadata.ContainerType == typeof(IEnumerable<IFormFile>))
                    .ToList();
                if (parameterDescriptionList.Count == 0) return;

                var parameters = operation
                    .Parameters
                    .Where(x => parameterDescriptionList.Exists(p => p.Name == x.Name))
                    .ToList();
                if (parameters.Count == 0) return;

                foreach (var parameter in parameters)
                {
                    operation.Parameters.Remove(parameter);
                }

                var required = parameters.FirstOrDefault()?.Required ?? false;
                operation.Parameters.Add(new OpenApiParameter()
                {
                    Name = "attachments",
                    In = ParameterLocation.Query,
                    Description = "Upload Files.",
                    Required = required
                });
            }
        }

        private void ConfigureServicesSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Web API",
                    Description = "Web API",
                    Contact = new OpenApiContact
                    {
                        Name = "Exemplo",
                        Email = string.Empty,
                        Url = new Uri("https://exemplo.com")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under LICX",
                        Url = new Uri("https://exemplo.com")
                    }
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme 
                {
                    In = ParameterLocation.Header,
                    Description = "Please enter JWT with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme()
                        {
                            Reference = new OpenApiReference()
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer",
                            }
                        },
                        new string[] {}
                    }
                });

                c.CustomSchemaIds(x => x.Name.Contains("`") ? string.Format("{0}<{1}>", x.Name.Split("`")[0], string.Join(", ", x.GenericTypeArguments.Select(a => a.Name))) : x.Name);

                c.OperationFilter<DownloadFileFilter>();
                c.OperationFilter<UploadFileFilter>();
                c.OperationFilter<UploadFileListFilter>();

                c.MapType<FileStream>(() => new OpenApiSchema {Title = "File", Description = "Download File", Type = "File"});

                c.DescribeAllEnumsAsStrings();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        private void ConfigureSwagger(IApplicationBuilder app)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Exemplo API V1");
            });
        }
    }
}
